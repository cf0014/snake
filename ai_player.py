from snake import GameOutcome

'''
AI-Player. This module contains:
- SystemState: it captures one-ring vision of the snake
- AI_Player0: base class for all AI player. This class doesn't control the snake,
              it can be used for human player & leaves the control of snake to the
              main loop to implement
- AI_RuleBased: it controls the snake using some predefined rules
---------
'''

'''
System State class
'''
class SystemState:
    def __init__(self):
        ## mark the position of the food relative to the snake
        self.food_north: bool = False
        self.food_south: bool = False
        self.food_east: bool = False
        self.food_west: bool = False
        ## mark the obstable one-ring around the snake
        self.obj_north: int = 0
        self.obj_south: int = 0
        self.obj_east: int = 0
        self.obj_west: int = 0
        self.obj_north_east: int = 0
        self.obj_north_west: int = 0
        self.obj_south_east: int = 0
        self.obj_south_west: int = 0
        ## record the current movement of the snake
        self.dir_x: int = 0
        self.dir_y: int = 0


'''
This is the parent class for one-ring system state
- callback_take_action() is called when the environment requests 
  your agent to take an action based on the current state
- callback_action_outcome() is called right after the environment
  has taken the last action to report the new state and outcome
- callback_terminating() is called right when the program is just about
  to exit. The algorithm can print some final statistical info or save some info
  before the program ends
'''
class AI_Player0:

    def __init__(self):
        self._name = "Human Player"
        self._state: SystemState = None

    ## return the name
    def get_name(self) -> str:
        return self._name

    ## return system state visible by this player
    def state_str(self, state:SystemState) -> str:
        return "["+(">" if state.food_east else " ") \
                  +("v" if state.food_south else " ") \
                  +("<" if state.food_west else " ") \
                  +("^" if state.food_north else " ") + "]," \
                  + "[%+d,%+d,%+d,%d]"% \
                      (state.obj_north,state.obj_south,state.obj_east,state.obj_west) \
                  + "-%s"%("U" if state.dir_y==-1 else "D" if state.dir_y==1 else \
                           "L" if state.dir_x==-1 else "R")

    ## return if keyboard input is allowed
    def is_keyboard_allowed(self) -> bool:
        return True # by default, we allow so human user can control

    ## callback when a request for action is needed by the environment
    def callback_take_action(self, state:SystemState) -> (int,int):
        return (state.dir_x,state.dir_y) # return same movement by default

    ## callback when the outcome for the last action is available
    def callback_action_outcome(self, state:SystemState, outcome:GameOutcome):
        pass # do nothing by default, ignore the outcome

    ## callback when the environment is terminating
    def callback_terminating(self):
        pass # do nothing by default

'''
This is algorithm 1: rule-based using one-ring snake vision
'''
class AI_RuleBased(AI_Player0):

    def __init__(self):
        super().__init__()
        self._name = "Rule-based algorithm"

    ## return if keyboard input is allowed
    def is_keyboard_allowed(self) -> bool:
        return False # this is played by algo, don't allow keyboard input

    ## callback when a request for action is needed by the environment
    ## return the movement decision based on the algorithm
    def callback_take_action(self, state:SystemState) -> (int,int):
        BLOCKED = -1
        EAST = (+1,0); WEST = (-1,0)
        NORTH = (0,-1); SOUTH = (0,+1)
        movement = (state.dir_x,state.dir_y)
        if state.food_north and state.obj_north is not BLOCKED:   movement = NORTH 
        elif state.food_south and state.obj_south is not BLOCKED: movement = SOUTH 
        elif state.food_east and state.obj_east is not BLOCKED:   movement = EAST 
        elif state.food_west and state.obj_west is not BLOCKED:   movement = WEST
        else: # the following is where the snake can't move towards
              # the food & has to go around
            if state.dir_x!=0: # for east/west movement
                if state.obj_north is not BLOCKED:    movement = NORTH 
                elif state.obj_south is not BLOCKED:  movement = SOUTH 
            elif state.dir_y!=0: # for north/south movement
                if state.obj_east is not BLOCKED:     movement = EAST 
                elif state.obj_west is not BLOCKED:   movement = WEST 
            else: pass # by passing, the snake will continue its current movement
        return movement

'''
This is helper class: a floating point data type with a built-in decaying function
'''
class DecayingFloat:
    def __init__(self, value:float, factor:float=None, minval:float=None, \
                    mode:str="exp"):
        self.init = value
        self.value = value
        self.factor = factor
        self.minval = minval
        self.mode = mode

    ## return as a float
    def __float__(self) -> float:
        return float(self.value)

    ## reset this value
    def reset(self):
        self.value = self.init

    ## apply a step of decay to this value
    def decay(self):
        if self.factor==None: return

        if self.mode=="exp":      self.value *= self.factor
        elif self.mode=="linear": self.value -= self.factor
        
        if self.minval==None: 
            return
        elif self.value<self.minval:
            self.value = self.minval
