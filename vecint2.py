# to cope with forward declaration for type annotation
# the following works for Python 3.7+
# expect to become a default in Python 3.10
from __future__ import annotations

import math

""" 
Supporting class, to deal with (x,y)-coordinate in integer
"""

class VecInt2:

    def __init__(self, x:int=0, y:int=0):
        self.x: int = x
        self.y: int = y

    ## operator+ to add 'other'
    def __add__(self, other):
        return VecInt2(self.x+other.x, self.y+other.y)

    ## operator- to substract 'other'
    def __sub__(self, other):
        return VecInt2(self.x-other.x, self.y-other.y)

    ## method to directly set its (x,y)
    def set_xy(self, x:int, y:int):
        self.x = x
        self.y = y

    ## method to set its (x,y) to 'p'
    def set_to(self, p:VecInt2):
        self.x = p.x
        self.y = p.y

    ## method to linearly move it by 'p'
    def move(self, p:VecInt2):
        self.x += p.x
        self.y += p.y

    ## return its distance to 'p'
    def distance_to(self, p:VecInt2) -> float:
        p2: VecInt2 = self - p
        return float(math.sqrt(p2.x**2 + p2.y**2))

    ## return (x,y) tuple
    def xy(self):
        return (self.x,self.y)

    ## check if it has the same (x,y) as 'p'
    def is_same_loc_as(self, p:VecInt2) -> bool:
        if self.x==p.x and self.y==p.y:
            return True
        return False